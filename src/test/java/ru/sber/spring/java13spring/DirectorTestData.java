package ru.sber.spring.java13spring;

import com.sber.java13spring.java13springproject.film.dto.DirectorDTO;
import com.sber.java13spring.java13springproject.film.model.Directors;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public interface DirectorTestData {
    DirectorDTO AUTHOR_DTO_1 = new DirectorDTO("directorFio1",
                                           1,
                                           new HashSet<>(),
                                           false);

    DirectorDTO AUTHOR_DTO_2 = new DirectorDTO("directorFio2",
            2,
            new HashSet<>(),
            false);

    DirectorDTO AUTHOR_DTO_3_DELETED = new DirectorDTO("directorFio3",
            3,
            new HashSet<>(),
            false);
    
    List<DirectorDTO> AUTHOR_DTO_LIST = Arrays.asList(AUTHOR_DTO_1, AUTHOR_DTO_2, AUTHOR_DTO_3_DELETED);


    Directors AUTHOR_1 = new Directors("directorFio1",
                                 1,
                                 null);

    Directors AUTHOR_2 = new Directors("directorFio1",
                                 2,
                                 null);

    Directors AUTHOR_3 = new Directors("directorFio1",
                                 3
                                 ,null);
    
    List<Directors> AUTHOR_LIST = Arrays.asList(AUTHOR_1, AUTHOR_2, AUTHOR_3);
}
