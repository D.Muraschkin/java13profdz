package ru.sber.spring.java13spring;



import com.sber.java13spring.java13springproject.film.dto.FilmRentInfoDTO;
import com.sber.java13spring.java13springproject.film.model.FilmRentInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;

import java.time.LocalDateTime;
import java.util.List;

public interface FilmRentInfoTestData {

   FilmRentInfoDTO FILM_RENT_INFO_DTO = new FilmRentInfoDTO(LocalDateTime.now(),
                                                             LocalDateTime.now(),
                                                             false,
                                                             14,
                                                             1L,
                                                             1L,
                                                             null);

    List<FilmRentInfoDTO> FILM_RENT_INFO_DTO_LIST = List.of(FILM_RENT_INFO_DTO);

    FilmRentInfo FILM_RENT_INFO = new FilmRentInfo(null,
                                                   null,
                                                   LocalDateTime.now(),
                                                   LocalDateTime.now(),
                                                   false,
                                                   14);

    List<FilmRentInfo> FILM_RENT_INFO_LIST = List.of(FILM_RENT_INFO);
}
