package ru.sber.spring.java13spring;

import com.sber.java13spring.java13springproject.film.dto.OrderDTO;
import com.sber.java13spring.java13springproject.film.model.Orders;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderTestData {
    Orders ORDERS = new Orders(
            null,
            null,
            LocalDateTime.now(),
            1,
            false
    );
    List<Orders> ORDERS_LIST = List.of(ORDERS);

    OrderDTO ORDER_DTO = new OrderDTO(
            null,
            null,
            LocalDateTime.now(),
            1,
            false,
            2L,
            3L
    );
    List<OrderDTO> ORDERS_LIST_DTO = List.of(ORDER_DTO);
}
