package ru.sber.spring.java13spring.Film.service;

import com.sber.java13spring.java13springproject.film.dto.FilmRentInfoDTO;
import com.sber.java13spring.java13springproject.film.mapper.FilmRentInfoMapper;
import com.sber.java13spring.java13springproject.film.model.FilmRentInfo;
import com.sber.java13spring.java13springproject.film.exception.MyDeleteException;
import com.sber.java13spring.java13springproject.film.repository.FilmRentInfoRepository;
import com.sber.java13spring.java13springproject.film.service.FilmRentInfoService;
import com.sber.java13spring.java13springproject.film.service.FilmService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import ru.sber.spring.java13spring.FilmRentInfoTestData;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class FilmRentInfoServiceTest extends GenericTest<FilmRentInfo, FilmRentInfoDTO> {

    public FilmRentInfoServiceTest() {
        super();
        repository = Mockito.mock(FilmRentInfoRepository.class);
        mapper = Mockito.mock(FilmRentInfoMapper.class);
        FilmService filmService = Mockito.mock(FilmService.class);
        service = new FilmRentInfoService((FilmRentInfoRepository) repository,
                (FilmRentInfoMapper) mapper,
                (FilmService) filmService
        );
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(FilmRentInfoTestData.FILM_RENT_INFO_LIST);
        Mockito.when(mapper.toDTOs(FilmRentInfoTestData.FILM_RENT_INFO_LIST)).thenReturn(FilmRentInfoTestData.FILM_RENT_INFO_DTO_LIST);
        List<FilmRentInfoDTO> filmRentInfoDTOS = service.listAll();
        log.info("Testing getAll(): " + filmRentInfoDTOS);
        assertEquals(FilmRentInfoTestData.FILM_RENT_INFO_LIST.size(), filmRentInfoDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(FilmRentInfoTestData.FILM_RENT_INFO));
        Mockito.when(mapper.toDTO(FilmRentInfoTestData.FILM_RENT_INFO)).thenReturn(FilmRentInfoTestData.FILM_RENT_INFO_DTO);
        FilmRentInfoDTO filmRentInfoDTO = service.getOne(1L);
        log.info("Testing getOne(): " + filmRentInfoDTO);
        assertEquals(FilmRentInfoTestData.FILM_RENT_INFO_DTO, filmRentInfoDTO);
    }

    @Test
    @Order(3)
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(FilmRentInfoTestData.FILM_RENT_INFO_DTO)).thenReturn(FilmRentInfoTestData.FILM_RENT_INFO);
        Mockito.when(mapper.toDTO(FilmRentInfoTestData.FILM_RENT_INFO)).thenReturn(FilmRentInfoTestData.FILM_RENT_INFO_DTO);
        Mockito.when(repository.save(FilmRentInfoTestData.FILM_RENT_INFO)).thenReturn(FilmRentInfoTestData.FILM_RENT_INFO);
        FilmRentInfoDTO filmRentInfoDTO = service.create(FilmRentInfoTestData.FILM_RENT_INFO_DTO);
        log.info("Testing create(): " + filmRentInfoDTO);
        assertEquals(FilmRentInfoTestData.FILM_RENT_INFO_DTO, filmRentInfoDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(FilmRentInfoTestData.FILM_RENT_INFO_DTO)).thenReturn(FilmRentInfoTestData.FILM_RENT_INFO);
        Mockito.when(mapper.toDTO(FilmRentInfoTestData.FILM_RENT_INFO)).thenReturn(FilmRentInfoTestData.FILM_RENT_INFO_DTO);
        Mockito.when(repository.save(FilmRentInfoTestData.FILM_RENT_INFO)).thenReturn(FilmRentInfoTestData.FILM_RENT_INFO);
        FilmRentInfoDTO filmRentInfoDTO = service.update(FilmRentInfoTestData.FILM_RENT_INFO_DTO);
        log.info("Testing update(): " + filmRentInfoDTO);
        assertEquals(FilmRentInfoTestData.FILM_RENT_INFO_DTO, filmRentInfoDTO);
    }

    @Test
    @Order(5)
    @Override
    protected void delete() throws MyDeleteException {
        Mockito.when(repository.save(FilmRentInfoTestData.FILM_RENT_INFO)).thenReturn(FilmRentInfoTestData.FILM_RENT_INFO);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(FilmRentInfoTestData.FILM_RENT_INFO));
        log.info("Testing delete() before: " + FilmRentInfoTestData.FILM_RENT_INFO.isDeleted());
        service.deleteSoft(1L);
        log.info("Testing delete() after: " + FilmRentInfoTestData.FILM_RENT_INFO.isDeleted());
        assertTrue(FilmRentInfoTestData.FILM_RENT_INFO.isDeleted());
    }
    @Test
    @Order(6)
    @Override
    protected void restore() {
//        FilmRentInfoTestData.FILM_RENT_INFO.setDeleted(true);
//        Mockito.when(repository.save(FilmRentInfoTestData.FILM_RENT_INFO)).thenReturn(FilmRentInfoTestData.FILM_RENT_INFO);
//        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(FilmRentInfoTestData.FILM_RENT_INFO));
//        log.info("Testing restore() before: " + FilmRentInfoTestData.FILM_RENT_INFO.isDeleted());
//        ((FilmRentInfoTestData) service).restore(3L);
//        log.info("Testing restore() after: " + FilmRentInfoTestData.FILM_RENT_INFO.isDeleted());
//        assertFalse(FilmRentInfoTestData.FILM_RENT_INFO.isDeleted());
    }


}
