package ru.sber.spring.java13spring.Film.service;


import com.sber.java13spring.java13springproject.film.dto.OrderDTO;
import com.sber.java13spring.java13springproject.film.exception.MyDeleteException;
import com.sber.java13spring.java13springproject.film.mapper.OrderMapper;
import com.sber.java13spring.java13springproject.film.model.Orders;
import com.sber.java13spring.java13springproject.film.repository.FilmRepository;
import com.sber.java13spring.java13springproject.film.repository.OrderInfoRepository;
import com.sber.java13spring.java13springproject.film.repository.UserRepository;
import com.sber.java13spring.java13springproject.film.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import ru.sber.spring.java13spring.OrderTestData;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class OrderServiceTest extends GenericTest<Orders, OrderDTO> {
    public OrderServiceTest() {
        super();
        repository = Mockito.mock(OrderInfoRepository.class);
        mapper = Mockito.mock(OrderMapper.class);
        UserRepository userRepository = Mockito.mock(UserRepository.class) ;
        FilmRepository filmRepository= Mockito.mock(FilmRepository.class);
        service = new OrderService((OrderInfoRepository) repository,
                (OrderMapper)  mapper,
                (UserRepository) userRepository,
                (FilmRepository) filmRepository
        );
    }
    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(OrderTestData.ORDERS_LIST);
        Mockito.when(mapper.toDTOs(OrderTestData.ORDERS_LIST)).thenReturn(OrderTestData.ORDERS_LIST_DTO);
        List<OrderDTO> orderDTOS = service.listAll();
        log.info("Testing getAll(): " + orderDTOS);
        assertEquals(OrderTestData.ORDERS_LIST.size(), orderDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(OrderTestData.ORDERS));
        Mockito.when(mapper.toDTO(OrderTestData.ORDERS)).thenReturn(OrderTestData.ORDER_DTO);
        OrderDTO orderDTOS = service.getOne(1L);
        log.info("Testing getOne(): " + orderDTOS);
        assertEquals(OrderTestData.ORDER_DTO, orderDTOS);
    }

    @Test
    @Order(3)
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(OrderTestData.ORDER_DTO)).thenReturn(OrderTestData.ORDERS);
        Mockito.when(mapper.toDTO(OrderTestData.ORDERS)).thenReturn(OrderTestData.ORDER_DTO);
        Mockito.when(repository.save(OrderTestData.ORDERS)).thenReturn(OrderTestData.ORDERS);
        OrderDTO orderDTOS = service.create(OrderTestData.ORDER_DTO);
        log.info("Testing create(): " + orderDTOS);
        assertEquals(OrderTestData.ORDER_DTO, orderDTOS);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(OrderTestData.ORDER_DTO)).thenReturn(OrderTestData.ORDERS);
        Mockito.when(mapper.toDTO(OrderTestData.ORDERS)).thenReturn(OrderTestData.ORDER_DTO);
        Mockito.when(repository.save(OrderTestData.ORDERS)).thenReturn(OrderTestData.ORDERS);
        OrderDTO orderDTOS= service.update(OrderTestData.ORDER_DTO);
        log.info("Testing update(): " + orderDTOS);
        assertEquals(OrderTestData.ORDER_DTO, orderDTOS);
    }

    @Test
    @Order(5)
    @Override
    protected void delete() throws MyDeleteException {
        Mockito.when(repository.save(OrderTestData.ORDERS)).thenReturn(OrderTestData.ORDERS);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(OrderTestData.ORDERS));
        log.info("Testing delete() before: " + OrderTestData.ORDERS.isDeleted());
        service.deleteSoft(1L);
        log.info("Testing delete() after: " + OrderTestData.ORDERS.isDeleted());
        assertTrue(OrderTestData.ORDERS.isDeleted());
    }

    @Test
    @Order(6)
    @Override
    protected void restore() {
//        OrderTestData.ORDERS.setDeleted(true);
//        Mockito.when(repository.save(OrderTestData.ORDERS)).thenReturn(OrderTestData.ORDERS);
//        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(OrderTestData.ORDERS));
//        log.info("Testing restore() before: " + OrderTestData.ORDERS.isDeleted());
//        ((OrderTestData) service).restore(3L);
//        log.info("Testing restore() after: " + OrderTestData.ORDERS.isDeleted());
//        assertFalse(OrderTestData.ORDERS.isDeleted());
    }

}
