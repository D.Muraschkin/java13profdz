package ru.sber.spring.java13spring.Film.service;



import com.sber.java13spring.java13springproject.film.dto.AddFilmDTO;
import com.sber.java13spring.java13springproject.film.dto.DirectorDTO;
import com.sber.java13spring.java13springproject.film.exception.MyDeleteException;
import com.sber.java13spring.java13springproject.film.mapper.DirectorMapper;
import com.sber.java13spring.java13springproject.film.mapper.DirectorWithFilmsMapper;
import com.sber.java13spring.java13springproject.film.model.Directors;
import com.sber.java13spring.java13springproject.film.repository.DirectorRepository;
import com.sber.java13spring.java13springproject.film.repository.FilmRepository;
import com.sber.java13spring.java13springproject.film.service.DirectorService;
import com.sber.java13spring.java13springproject.film.service.FilmService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import ru.sber.spring.java13spring.DirectorTestData;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class DirectorServiceTest
      extends GenericTest<Directors, DirectorDTO> {

//    private final DirectorRepository directorRepository = Mockito.mock(DirectorRepository.class);
//    private final DirectorMapper directorMapper = Mockito.mock(DirectorMapper.class);
    FilmRepository filmRepository;
    DirectorWithFilmsMapper directorWithFilmsMapper;
    public DirectorServiceTest() {
        super();
        FilmService filmService = Mockito.mock(FilmService.class);
        repository = Mockito.mock(DirectorRepository.class);
        mapper = Mockito.mock(DirectorMapper.class);
        filmRepository = Mockito.mock(FilmRepository.class);
        directorWithFilmsMapper=Mockito.mock(DirectorWithFilmsMapper.class);
        service = new DirectorService((DirectorMapper) mapper,
                (DirectorRepository) repository,
                (FilmRepository) filmRepository ,
                (DirectorWithFilmsMapper) directorWithFilmsMapper,
                (FilmService) filmService);
    }
    
    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(DirectorTestData.AUTHOR_LIST);
        Mockito.when(mapper.toDTOs(DirectorTestData.AUTHOR_LIST)).thenReturn(DirectorTestData.AUTHOR_DTO_LIST);
        List<DirectorDTO> directorDTOS = service.listAll();
//        System.out.println(directorDTOS);
        log.info("Testing getAll(): " + directorDTOS);
        assertEquals(DirectorTestData.AUTHOR_LIST.size(), directorDTOS.size());
    }
    
    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorTestData.AUTHOR_1));
        Mockito.when(mapper.toDTO(DirectorTestData.AUTHOR_1)).thenReturn(DirectorTestData.AUTHOR_DTO_1);
        DirectorDTO directorDTO = service.getOne(1L);
        log.info("Testing getOne(): " + directorDTO);
        assertEquals(DirectorTestData.AUTHOR_DTO_1, directorDTO);
    }

    @Order(3)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(DirectorTestData.AUTHOR_DTO_1)).thenReturn(DirectorTestData.AUTHOR_1);
        Mockito.when(mapper.toDTO(DirectorTestData.AUTHOR_1)).thenReturn(DirectorTestData.AUTHOR_DTO_1);
        Mockito.when(repository.save(DirectorTestData.AUTHOR_1)).thenReturn(DirectorTestData.AUTHOR_1);
        DirectorDTO directorDTO = service.create(DirectorTestData.AUTHOR_DTO_1);
        log.info("Testing create(): " + directorDTO);
        assertEquals(DirectorTestData.AUTHOR_DTO_1, directorDTO);
    }

    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(DirectorTestData.AUTHOR_DTO_1)).thenReturn(DirectorTestData.AUTHOR_1);
        Mockito.when(mapper.toDTO(DirectorTestData.AUTHOR_1)).thenReturn(DirectorTestData.AUTHOR_DTO_1);
        Mockito.when(repository.save(DirectorTestData.AUTHOR_1)).thenReturn(DirectorTestData.AUTHOR_1);
        DirectorDTO directorDTO = service.update(DirectorTestData.AUTHOR_DTO_1);
        log.info("Testing update(): " + directorDTO);
        assertEquals(DirectorTestData.AUTHOR_DTO_1, directorDTO);
    }



    @Order(5)
    @Test
    @Override
    protected void delete() throws MyDeleteException {
//        Mockito.when(((DirectorRepository) repository).checkDirectorForDeletion(1L)).thenReturn(true);
//        Mockito.when(authorRepository.checkAuthorForDeletion(2L)).thenReturn(false);
        Mockito.when(repository.save(DirectorTestData.AUTHOR_1)).thenReturn(DirectorTestData.AUTHOR_1);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorTestData.AUTHOR_1));
        log.info("Testing delete() before: " + DirectorTestData.AUTHOR_1.isDeleted());
        service.deleteSoft(1L);
        log.info("Testing delete() after: " + DirectorTestData.AUTHOR_1.isDeleted());
        assertTrue(DirectorTestData.AUTHOR_1.isDeleted());
    }

    @Order(6)
    @Test
    @Override
    protected void restore() {
        DirectorTestData.AUTHOR_3.setDeleted(true);
        Mockito.when(repository.save(DirectorTestData.AUTHOR_3)).thenReturn(DirectorTestData.AUTHOR_3);
        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(DirectorTestData.AUTHOR_3));
        log.info("Testing restore() before: " + DirectorTestData.AUTHOR_3.isDeleted());
        ((DirectorService) service).restore(3L);
        log.info("Testing restore() after: " + DirectorTestData.AUTHOR_3.isDeleted());
        assertFalse(DirectorTestData.AUTHOR_3.isDeleted());

    }

    @Order(7)
    @Test
    void searchAuthors() {
//        PageRequest pageRequest = PageRequest.of(1, 10, Sort.by(Sort.Direction.ASC, "authorFio"));
//        Mockito.when(((DirectorRepository) repository).findAllDirectorFioContainsIgnoreCaseAndIsDeletedFalse("authorFio1", pageRequest))
//              .thenReturn(new PageImpl<>(DirectorTestData.AUTHOR_LIST));
//        Mockito.when(mapper.toDTOs(DirectorTestData.AUTHOR_LIST)).thenReturn(DirectorTestData.AUTHOR_DTO_LIST);
//        Page<DirectorDTO> authorDTOList = ((DirectorService) service).searchDirectors("authorFio1", pageRequest);
//        log.info("Testing searchAuthors(): " + authorDTOList);
//        assertEquals(DirectorTestData.AUTHOR_DTO_LIST, authorDTOList.getContent());
    }

    @Order(8)
    @Test
    void addFilm() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorTestData.AUTHOR_1));
        Mockito.when(service.getOne(1L)).thenReturn(DirectorTestData.AUTHOR_DTO_1);
        Mockito.when(repository.save(DirectorTestData.AUTHOR_1)).thenReturn(DirectorTestData.AUTHOR_1);
        ((DirectorService) service).addFilm1(new AddFilmDTO(1L, 1L));
        log.info("Testing addFilm(): " + DirectorTestData.AUTHOR_DTO_1.getFilmsIds());
        assertTrue(DirectorTestData.AUTHOR_DTO_1.getFilmsIds().size() >= 1);
    }


    
}
