package ru.sber.spring.java13spring.Film.service;



import com.sber.java13spring.java13springproject.film.dto.UserDTO;
import com.sber.java13spring.java13springproject.film.exception.MyDeleteException;
import com.sber.java13spring.java13springproject.film.mapper.FilmMapper;
import com.sber.java13spring.java13springproject.film.mapper.UserMapper;
import com.sber.java13spring.java13springproject.film.model.User;
import com.sber.java13spring.java13springproject.film.repository.UserRepository;
import com.sber.java13spring.java13springproject.film.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import ru.sber.spring.java13spring.UserTestData;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class UserServiceTest extends GenericTest<User, UserDTO> {

    public UserServiceTest() {
        super();
        repository = Mockito.mock(UserRepository.class);
        mapper = Mockito.mock(UserMapper.class);
        FilmMapper filmMapper = Mockito.mock(FilmMapper.class);
        BCryptPasswordEncoder bCryptPasswordEncoder = Mockito.mock(BCryptPasswordEncoder.class);
        service = new UserService((UserRepository) repository,
                (UserMapper) mapper,
                (FilmMapper) filmMapper ,
                (BCryptPasswordEncoder) bCryptPasswordEncoder
                );
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(UserTestData.USER_LIST);
        Mockito.when(mapper.toDTOs(UserTestData.USER_LIST)).thenReturn(UserTestData.USER_DTO_LIST);
        List<UserDTO> userDTOS = service.listAll();

        log.info("Testing getAll(): " + userDTOS);
        assertEquals(UserTestData.USER_LIST.size(), userDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(UserTestData.USER));
        Mockito.when(mapper.toDTO(UserTestData.USER)).thenReturn(UserTestData.USER_DTO);
        UserDTO userDTO = service.getOne(1L);
        log.info("Testing getOne(): " + userDTO);
        assertEquals(UserTestData.USER_DTO, userDTO);
    }

    @Test
    @Order(3)
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(UserTestData.USER_DTO)).thenReturn(UserTestData.USER);
        Mockito.when(mapper.toDTO(UserTestData.USER)).thenReturn(UserTestData.USER_DTO);
        Mockito.when(repository.save(UserTestData.USER)).thenReturn(UserTestData.USER);
        UserDTO userDTO = service.create(UserTestData.USER_DTO);
        log.info("Testing create(): " + userDTO);
        assertEquals(UserTestData.USER_DTO, userDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(UserTestData.USER_DTO)).thenReturn(UserTestData.USER);
        Mockito.when(mapper.toDTO(UserTestData.USER)).thenReturn(UserTestData.USER_DTO);
        Mockito.when(repository.save(UserTestData.USER)).thenReturn(UserTestData.USER);
        UserDTO userDTO = service.update(UserTestData.USER_DTO);
        log.info("Testing update(): " + userDTO);
        assertEquals(UserTestData.USER_DTO, userDTO);
    }

    @Test
    @Order(5)
    @Override
    protected void delete() throws MyDeleteException {
//        Mockito.when(((UserRepository) repository).checkDirectorForDeletion(1L)).thenReturn(true);
//        Mockito.when(authorRepository.checkAuthorForDeletion(2L)).thenReturn(false);
        Mockito.when(repository.save(UserTestData.USER)).thenReturn(UserTestData.USER);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(UserTestData.USER));
        log.info("Testing delete() before: " + UserTestData.USER.isDeleted());
        service.deleteSoft(1L);
        log.info("Testing delete() after: " + UserTestData.USER.isDeleted());
        assertTrue(UserTestData.USER.isDeleted());
    }

    @Test
    @Order(6)
    @Override
    protected void restore() {
//        UserTestData.USER.setDeleted(true);
//        Mockito.when(repository.save(UserTestData.USER)).thenReturn(UserTestData.USER);
//        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(UserTestData.USER));
//        log.info("Testing restore() before: " + UserTestData.USER.isDeleted());
//        ((UserService) service).restore(3L);
//        log.info("Testing restore() after: " + UserTestData.USER.isDeleted());
//        assertFalse(UserTestData.USER.isDeleted());
    }


}
