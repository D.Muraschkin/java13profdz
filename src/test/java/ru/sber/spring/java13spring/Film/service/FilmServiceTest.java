package ru.sber.spring.java13spring.Film.service;



import com.sber.java13spring.java13springproject.film.dto.FilmDTO;
import com.sber.java13spring.java13springproject.film.exception.MyDeleteException;
import com.sber.java13spring.java13springproject.film.mapper.FilmMapper;
import com.sber.java13spring.java13springproject.film.mapper.FilmWithDirectorsMapper;
import com.sber.java13spring.java13springproject.film.model.Film;
import com.sber.java13spring.java13springproject.film.repository.DirectorRepository;
import com.sber.java13spring.java13springproject.film.repository.FilmRepository;
import com.sber.java13spring.java13springproject.film.service.FilmService;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import ru.sber.spring.java13spring.FilmTestData;


import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class FilmServiceTest extends GenericTest<Film, FilmDTO> {

    public FilmServiceTest() {
        super();
        repository = Mockito.mock(FilmRepository.class);
        mapper = Mockito.mock(FilmMapper.class);
        DirectorRepository directorRepository = Mockito.mock(DirectorRepository.class);
        FilmWithDirectorsMapper filmWithDirectorsMapper = Mockito.mock( FilmWithDirectorsMapper.class);
        service = new FilmService((FilmRepository) repository,
                (FilmMapper) mapper,
                (DirectorRepository) directorRepository ,
                (FilmWithDirectorsMapper) filmWithDirectorsMapper
        );
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(FilmTestData.FILM_LIST);
        Mockito.when(mapper.toDTOs(FilmTestData.FILM_LIST)).thenReturn(FilmTestData.FILM_DTO_LIST);
        List<FilmDTO> filmDTOS = service.listAll();
        log.info("Testing getAll(): " + filmDTOS);
        assertEquals(FilmTestData.FILM_LIST.size(), filmDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(FilmTestData.FILM_1));
        Mockito.when(mapper.toDTO(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_DTO_1);
        FilmDTO filmDTO = service.getOne(1L);
        log.info("Testing getOne(): " + filmDTO);
        assertEquals(FilmTestData.FILM_DTO_1, filmDTO);
    }

    @Test
    @Order(3)
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(FilmTestData.FILM_DTO_1)).thenReturn(FilmTestData.FILM_1);
        Mockito.when(mapper.toDTO(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_DTO_1);
        Mockito.when(repository.save(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_1);
        FilmDTO filmDTO = service.create(FilmTestData.FILM_DTO_1);
        log.info("Testing create(): " + filmDTO);
        assertEquals(FilmTestData.FILM_DTO_1, filmDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(FilmTestData.FILM_DTO_1)).thenReturn(FilmTestData.FILM_1);
        Mockito.when(mapper.toDTO(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_DTO_1);
        Mockito.when(repository.save(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_1);
        FilmDTO filmDTO = service.update(FilmTestData.FILM_DTO_1);
        log.info("Testing update(): " + filmDTO);
        assertEquals(FilmTestData.FILM_DTO_1, filmDTO);
    }

    @Test
    @Order(5)
    @Override
    protected void delete() throws MyDeleteException {
        Mockito.when(repository.save(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_1);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(FilmTestData.FILM_1));
        log.info("Testing delete() before: " + FilmTestData.FILM_1.isDeleted());
        service.deleteSoft(1L);
        log.info("Testing delete() after: " + FilmTestData.FILM_1.isDeleted());
        assertTrue(FilmTestData.FILM_1.isDeleted());
    }

    @Test
    @Order(6)
    @Override
    protected void restore() {
//        FilmTestData.FILM_2.setDeleted(true);
//        Mockito.when(repository.save(FilmTestData.FILM_2)).thenReturn(FilmTestData.FILM_2);
//        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(FilmTestData.FILM_2));
//        log.info("Testing restore() before: " + FilmTestData.FILM_2.isDeleted());
//        ((FilmTestData) service).restore(3L);
//        log.info("Testing restore() after: " + FilmTestData.FILM_2.isDeleted());
//        assertFalse(FilmTestData.FILM_2.isDeleted());
    }


}
