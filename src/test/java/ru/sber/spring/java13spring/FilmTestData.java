package ru.sber.spring.java13spring;



import com.sber.java13spring.java13springproject.film.dto.DirectorDTO;
import com.sber.java13spring.java13springproject.film.dto.FilmDTO;
import com.sber.java13spring.java13springproject.film.dto.FilmWithDirectorsDTO;
import com.sber.java13spring.java13springproject.film.model.Film;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.sber.java13spring.java13springproject.film.model.Genre.DRAMA;
import static com.sber.java13spring.java13springproject.film.model.Genre.NOVEL;

public interface FilmTestData {
    FilmDTO FILM_DTO_1 = new FilmDTO("FilmTitle1",
                                     2001,
                                     "country",
                                     DRAMA,
                                     false,
                                     new HashSet<>(),
                                     new HashSet<>());

    FilmDTO FILM_DTO_2 = new FilmDTO("FilmTitle2",
            2002,
            "country",
            NOVEL,
            false,
            new HashSet<>(),
            new HashSet<>());

    List<FilmDTO> FILM_DTO_LIST = Arrays.asList(FILM_DTO_1, FILM_DTO_2);

    Film FILM_1 = new Film("FilmTitle1",
            2001,
            "country",
            DRAMA,
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>(),
            false,
            null,
            "qwe");
    Film FILM_2 = new Film("FilmTitle",
            2002,
            "country",
            NOVEL,
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>(),
            false,
            null,
            "qwe");

    List<Film> FILM_LIST = Arrays.asList(FILM_1, FILM_2);

    Set<DirectorDTO> DIRECTORS = new HashSet<>(DirectorTestData.AUTHOR_DTO_LIST);
    FilmWithDirectorsDTO FILM_WITH_DIRECTORS_DTO_1 = new FilmWithDirectorsDTO(FILM_1, DIRECTORS);
    FilmWithDirectorsDTO FILM_WITH_DIRECTORS_DTO_2 = new FilmWithDirectorsDTO(FILM_2, DIRECTORS);

    List<FilmWithDirectorsDTO> FILM_WITH_DIRECTORS_DTO_LIST = Arrays.asList(FILM_WITH_DIRECTORS_DTO_1, FILM_WITH_DIRECTORS_DTO_2);
}
