package ru.sber.spring.java13spring;



import com.sber.java13spring.java13springproject.film.dto.RoleDTO;
import com.sber.java13spring.java13springproject.film.dto.UserDTO;
import com.sber.java13spring.java13springproject.film.model.Role;
import com.sber.java13spring.java13springproject.film.model.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.List;

public interface UserTestData {

    UserDTO USER_DTO = new UserDTO(
          "login",
          "password",
          "firstName",
          "lastName",
          "middleName",
            "birthDate",
          "phone",
          "address",
            "email",
            LocalDate.now().atStartOfDay(),
          new RoleDTO(),
          "changePasswordToken",
          new HashSet<>(),
          false
    );

    List<UserDTO> USER_DTO_LIST = List.of(USER_DTO);

    User USER = new User(
          "login",
          "password",
          "firstName",
          "lastName",
          "middleName",
            LocalDate.now(),
          "phone",
          "address",
            "email",
            LocalDate.now().atStartOfDay(),
//          "changePasswordToken",
          new Role(),
          new HashSet<>()
    );

    List<User> USER_LIST = List.of(USER);
}
