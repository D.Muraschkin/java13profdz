package com.sber.java13spring.java13springproject.dbexample.model;

import lombok.*;

@Data
//@Getter
//@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    @Setter(AccessLevel.NONE)
    private Integer Id;
    private String surName;
    private String name;
    private String dateOfBirth;
    private String phone;
    private String mail;
    private String listBook;


    public User(String surName, String name, String dateOfBirth, String phone, String mail, String listBook) {
        this.surName = surName;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.phone = phone;
        this.mail = mail;
        this.listBook = listBook;
    }

}
