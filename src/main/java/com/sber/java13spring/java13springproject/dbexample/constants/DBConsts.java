package com.sber.java13spring.java13springproject.dbexample.constants;

public interface DBConsts {
    String DB_HOST = "localhost";
    String DB = "local_db_dz";
    String USER = "postgresdz";
    String PASSWORD = "12345";
    String PORT = "5432";
}
