package com.sber.java13spring.java13springproject.dbexample.dao;

import com.sber.java13spring.java13springproject.dbexample.model.User;

import java.sql.*;

//@Component
public class UserDAO {
    private Connection connection ;

    public UserDAO(Connection connection) {
        this.connection = connection;
    }

    public void createTableUser() throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("create table usersDZ (\n" +
                "id serial primary key,\n" +
                "surname varchar(50) not null,\n" +
                "name varchar(50) not null,\n" +
                "dateOfBirth varchar(50) ,\n" +
                "phone varchar(50) ,\n" +
                "mail varchar(100) ,\n" +
                "listBook varchar(100) )");
    }

    public void dropTableUser() throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("drop table usersDZ");
    }

    public void createUsers(User user) throws SQLException {
        PreparedStatement insert = connection.prepareStatement("insert into usersDZ (surName, name,dateOfBirth, phone, mail, listBook) values (?,?,?,?,?,?)");
            insert.setString(1, user.getSurName());
            insert.setString(2, user.getName());
            insert.setString(3, user.getDateOfBirth());
            insert.setString(4, user.getPhone());
            insert.setString(5, user.getMail());
            insert.setString(6, user.getListBook());
             insert.executeUpdate();

    }
//    public List<String> informationBook(String s) throws SQLException {
//
//  }

}
