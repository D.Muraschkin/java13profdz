package com.sber.java13spring.java13springproject.film.service;

import com.sber.java13spring.java13springproject.film.dto.AddFilmDTO;
import com.sber.java13spring.java13springproject.film.dto.DirectorDTO;
import com.sber.java13spring.java13springproject.film.dto.DirectorWithFilmsDTO;
import com.sber.java13spring.java13springproject.film.dto.FilmWithDirectorsDTO;
import com.sber.java13spring.java13springproject.film.mapper.DirectorMapper;
import com.sber.java13spring.java13springproject.film.mapper.DirectorWithFilmsMapper;
import com.sber.java13spring.java13springproject.film.model.Directors;
import com.sber.java13spring.java13springproject.film.model.Film;
import com.sber.java13spring.java13springproject.film.repository.DirectorRepository;
import com.sber.java13spring.java13springproject.film.repository.FilmRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;
import java.util.Set;

@Service
public class   DirectorService extends GenericService<Directors, DirectorDTO> {
    protected final FilmRepository filmRepository;
    protected final FilmService filmService;
    private final DirectorRepository directorRepository;
    private final DirectorWithFilmsMapper directorWithFilmsMapper;

    public DirectorService(DirectorMapper directorMapper,
                           DirectorRepository directorRepository,
                           FilmRepository filmRepository,
                           DirectorWithFilmsMapper directorWithFilmsMapper
                           ,FilmService filmService) {
        super(directorRepository, directorMapper);
        this.filmRepository = filmRepository;
        this.directorRepository = directorRepository;
        this.directorWithFilmsMapper = directorWithFilmsMapper;
        this.filmService = filmService;
    }

//    public DirectorService(DirectorRepository directorRepository, DirectorMapper directorMapper) {
//        super(directorRepository, directorMapper);
//    }
//public Page<DirectorDTO> searchDirectors(final String fio,
//                                     Pageable pageable) {
//    Page<Directors> directors = DirectorRepository.findAllDirectorFioContainsIgnoreCaseAndIsDeletedFalse(fio, pageable);
//    List<DirectorDTO> result = mapper.toDTOs(directors.getContent());
//    return new PageImpl<>(result, pageable, directors.getTotalElements());
//}

    //Сервис "Добавить фильм к режиссёру"
    public DirectorDTO addFilm(Long directorId, Long filmId) {
        Directors director = directorRepository.findById(directorId)
                .orElseThrow(() -> new NotFoundException("Режиссёр с таким ID не найден"));
        Film film = filmRepository.findById(filmId)
                .orElseThrow(() -> new NotFoundException("Фильм с таким ID не найден"));
        director.getFilms().add(film);
        return mapper.toDTO(directorRepository.save(director));
    }
    public void addFilm1(AddFilmDTO addBookDTO) {
        DirectorDTO author = getOne(addBookDTO.getAuthorId());
        filmService.getOne(addBookDTO.getBookId());
        author.getFilmsIds().add(addBookDTO.getBookId());
        update(author);
    }

    public List<DirectorWithFilmsDTO> getAllDirectorsWithFilms () {
        return directorWithFilmsMapper.toDTOs(directorRepository.findAll());
    }
    public void restore(Long objectId) {
        Directors director = directorRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Автора с заданным id=" + objectId + " не существует."));
        unMarkAsDeleted(director);
        Set<Film> films = director.getFilms();
        if (films != null && films.size() > 0) {
            films.forEach(this::unMarkAsDeleted);
        }
        directorRepository.save(director);
    }

}
