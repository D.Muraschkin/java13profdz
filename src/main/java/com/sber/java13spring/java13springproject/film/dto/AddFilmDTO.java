package com.sber.java13spring.java13springproject.film.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddFilmDTO {
    Long bookId;
    Long authorId;
}
