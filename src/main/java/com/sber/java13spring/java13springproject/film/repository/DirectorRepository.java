package com.sber.java13spring.java13springproject.film.repository;

import com.sber.java13spring.java13springproject.film.model.Directors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface DirectorRepository
      extends GenericRepository<Directors> {
//    Page<Directors> findAllByIsDeletedFalse(Pageable pageable);
//
//     Page<Directors> findAllDirectorFioContainsIgnoreCaseAndIsDeletedFalse(String fio,
//                                                                       Pageable pageable);

//    @Query(
//            value = """
//          select case when count(a) > 0 then false else true end
//          from Directors a join a.films b
//                        join FilmRentInfo bri on b.id = bri.film.id
//          where a.id = :directorId
//          and bri.returned = false
//          """
//    )
//    boolean checkDirectorForDeletion(final Long directorId);
}
