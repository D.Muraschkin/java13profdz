package com.sber.java13spring.java13springproject.film.controller;

import com.sber.java13spring.java13springproject.film.dto.DirectorDTO;
import com.sber.java13spring.java13springproject.film.dto.FilmDTO;
import com.sber.java13spring.java13springproject.film.model.Directors;
import com.sber.java13spring.java13springproject.film.service.DirectorService;
import com.sber.java13spring.java13springproject.film.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/directors")
@Tag(name = "Режиссёр",
     description = "Контроллер для работы с режиссёрами фильмов")
@SecurityRequirement(name = "Bearer Authentication")

//@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DirectorController
        extends GenericController<Directors, DirectorDTO> {
    private final DirectorService directorService;

    public DirectorController(DirectorService directorService) {
       super(directorService);
        this.directorService = directorService;

    }

    @Operation(description = "Добавить фильм к режиссёру", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "filmId") Long filmId,
                                               @RequestParam(value = "directorId") Long directorId) {


        return ResponseEntity.status(HttpStatus.OK).body(directorService.addFilm(directorId, filmId));
    }
}
