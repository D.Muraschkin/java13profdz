package com.sber.java13spring.java13springproject.film.controller;

import com.sber.java13spring.java13springproject.film.dto.DirectorDTO;
import com.sber.java13spring.java13springproject.film.dto.FilmDTO;
import com.sber.java13spring.java13springproject.film.model.Directors;
import com.sber.java13spring.java13springproject.film.model.Film;
import com.sber.java13spring.java13springproject.film.repository.DirectorRepository;
import com.sber.java13spring.java13springproject.film.repository.FilmRepository;
import com.sber.java13spring.java13springproject.film.service.DirectorService;
import com.sber.java13spring.java13springproject.film.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;


@RestController
@RequestMapping(value = "/film")
@Tag(name = "Фильмы",
        description = "Контроллер для работы с фильмами Фильмотеки")
public class FilmController extends GenericController<Film, FilmDTO> {
    private final FilmService filmService;


    public FilmController(FilmService filmService) {
        super(filmService);
        this.filmService = filmService;

    }

    @Operation(description = "Добавить режиссёра к фильму", method = "addDirector")
    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDTO> addDirector(@RequestParam(value = "filmId") Long filmId,
                                               @RequestParam(value = "directorId") Long directorId) {


        return ResponseEntity.status(HttpStatus.OK).body(filmService.addDirector(filmId, directorId));
    }

}
