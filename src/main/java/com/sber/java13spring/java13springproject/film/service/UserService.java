package com.sber.java13spring.java13springproject.film.service;

import com.sber.java13spring.java13springproject.film.dto.FilmDTO;
import com.sber.java13spring.java13springproject.film.dto.RoleDTO;
import com.sber.java13spring.java13springproject.film.dto.UserDTO;
import com.sber.java13spring.java13springproject.film.mapper.FilmMapper;
import com.sber.java13spring.java13springproject.film.mapper.UserMapper;
import com.sber.java13spring.java13springproject.film.model.Orders;
import com.sber.java13spring.java13springproject.film.model.User;
import com.sber.java13spring.java13springproject.film.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.sber.java13spring.java13springproject.film.constants.UserRolesConstants.ADMIN;


@Service
public class UserService extends GenericService<User, UserDTO> {

    private final FilmMapper filmMapper;
    private final UserRepository userRepository;
    private  BCryptPasswordEncoder bCryptPasswordEncoder ;

    public UserService(UserRepository userRepository,
                       UserMapper userMapper,
                       FilmMapper filmMapper
            , BCryptPasswordEncoder bCryptPasswordEncoder) {
        super(userRepository, userMapper);
        this.filmMapper = filmMapper;
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

//    Сервис "Получить список всех фильмов пользователя"
    public Set<FilmDTO> getFilmList(Long userId) {
        Set<FilmDTO> filmDTOS = new HashSet<>();
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("Пользователь с таким ID не найден"));
        Set<Orders> orders = user.getOrders();

        LocalDate today = LocalDate.now();
        for (Orders order : orders) {
            if (order.isPurchase() ||
                    today.isAfter(ChronoLocalDate.from(order.getRentDate().minusDays(1))) &&
                            today.isBefore(ChronoLocalDate.from(order.getRentDate().plusDays(order.getRentPeriod() + 1)))) {
                filmDTOS.add(filmMapper.toDTO(order.getFilm()));
            }
        }
        return filmDTOS;
    }


    public UserDTO getUserByLogin(final String login) {
        return mapper.toDTO (((UserRepository) repository).findUserByLogin(login));
    }

    public UserDTO getUserByEmail(final String email) {
        return mapper.toDTO(((UserRepository) repository).findUserByEmail(email));
    }

    public Boolean checkPassword(String password, UserDetails userDetails) {
        return bCryptPasswordEncoder.matches(password, userDetails.getPassword());
    }
    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if (ADMIN.equalsIgnoreCase(userName)) {
            roleDTO.setId(2L);//библиотекарь
        }
        else {
            roleDTO.setId(1L);
        }

        object.setRoles(roleDTO);
        object.setCreatedBy("REGISTRATION FORM");
        object.setCreatedWhen(LocalDateTime.now());
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public Page<UserDTO> findUsers(UserDTO userDTO,
                                   Pageable pageable) {
        Page<User> users = ((UserRepository) repository).searchUsers(userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getLogin(),
                pageable);
        List<UserDTO> result = mapper.toDTOs(users.getContent());
        return new PageImpl<>(result, pageable, users.getTotalElements());
    }
//    public void changePassword(final String uuid,
//                               final String password) {
//        UserDTO user = mapper.toDTO(((UserRepository) repository).findUserByChangePasswordToken(uuid));
//        user.setChangePasswordToken(null);
//        user.setPassword(bCryptPasswordEncoder.encode(password));
//        update(user);
//    }

}
