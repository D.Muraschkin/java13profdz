package com.sber.java13spring.java13springproject.film.config;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@SecurityScheme(
        name = "Bearer Authentication",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
public class OpenAPIConfig {
@Bean
    public OpenAPI FilmProject(){
       return new OpenAPI()
               .info(new Info().title("Фильмотека")
                       .description("Сервис, позволяющий получить фильм в фильмотеке.")
                       .version("v0.1")
                       .license(new License().name("Apache 2.0").url(""))
                       .contact(new Contact().name("D. Muraschkin")
                               .email("")
                               .url("")));
    }
}
