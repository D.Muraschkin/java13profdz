package com.sber.java13spring.java13springproject.film.repository;

import com.sber.java13spring.java13springproject.film.model.Orders;
import org.springframework.stereotype.Repository;


@Repository
public interface OrderInfoRepository
      extends GenericRepository<Orders> {
}
