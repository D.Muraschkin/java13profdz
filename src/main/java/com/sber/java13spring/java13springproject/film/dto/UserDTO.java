package com.sber.java13spring.java13springproject.film.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO extends GenericDTO {
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private String birthDate;
    private String phone;
    private String address;
    private String email;
    private
//    LocalDate
//    String
   LocalDateTime
            createdWhen;
    private RoleDTO roles;
    private String changePasswordToken;
    private Set<Long> ordersIds;
    private boolean isDeleted;
}
