package com.sber.java13spring.java13springproject.film.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
public class RoleDTO extends GenericDTO {
    private Long id;
    private String title;
    private String description;

//    private Set<Long> usersIds;
}
