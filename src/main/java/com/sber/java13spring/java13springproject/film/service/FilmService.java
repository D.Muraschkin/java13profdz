package com.sber.java13spring.java13springproject.film.service;

import com.sber.java13spring.java13springproject.film.dto.FilmDTO;
import com.sber.java13spring.java13springproject.film.dto.FilmSearchDTO;
import com.sber.java13spring.java13springproject.film.dto.FilmWithDirectorsDTO;
import com.sber.java13spring.java13springproject.film.mapper.FilmMapper;
import com.sber.java13spring.java13springproject.film.mapper.FilmWithDirectorsMapper;
import com.sber.java13spring.java13springproject.film.model.Directors;
import com.sber.java13spring.java13springproject.film.model.Film;
import com.sber.java13spring.java13springproject.film.repository.DirectorRepository;
import com.sber.java13spring.java13springproject.film.repository.FilmRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class FilmService extends GenericService<Film, FilmDTO> {
    private final FilmRepository filmRepository;
    protected final DirectorRepository directorRepository;
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;
    public FilmService(FilmRepository filmRepository,
                       FilmMapper filmMapper,
                       DirectorRepository directorRepository,
                       FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(filmRepository, filmMapper);
        this.filmRepository = filmRepository;
        this.directorRepository=directorRepository;
        this.filmWithDirectorsMapper =filmWithDirectorsMapper;
    }

    //Сервис "Добавить режиссёра к фильму"
    public FilmDTO addDirector(Long filmId, Long directorId) {
        Film film = filmRepository.findById(filmId)
                .orElseThrow(()->new NotFoundException("Фильм с таким ID не найден"));
        Directors director = directorRepository.findById(directorId)
                .orElseThrow(() -> new NotFoundException("Режиссёр с таким ID не найден"));
        film.getDirectors().add(director);
        return mapper.toDTO(filmRepository.save(film));
    }
    public Page<FilmWithDirectorsDTO> getAllFilmsWithDirectors(Pageable pageable) {
        Page<Film> filmsPaginated = filmRepository.findAll(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }
    public FilmWithDirectorsDTO getFilmsWithDirectors(Long id) {
        return filmWithDirectorsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    public Page<FilmWithDirectorsDTO> findFilm(FilmSearchDTO filmSearchDTO, Pageable pageable){
        String genre = filmSearchDTO.getGenre() != null ? String.valueOf(filmSearchDTO.getGenre().ordinal()) : null;
        Page<Film> filmsPaginated = filmRepository.searchFilms(genre,
                                                                             filmSearchDTO.getFilmTitle(),
                                                                             filmSearchDTO.getDirectorFio(),
                                                                             pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

}
