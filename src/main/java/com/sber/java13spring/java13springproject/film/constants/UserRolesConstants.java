package com.sber.java13spring.java13springproject.film.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String FILMLIBRARIAN = "FILMLIBRARIAN";
    String USER = "USER";
}
