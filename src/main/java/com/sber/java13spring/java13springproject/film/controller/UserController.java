package com.sber.java13spring.java13springproject.film.controller;

import com.sber.java13spring.java13springproject.film.config.jwt.JWTTokenUtil;
import com.sber.java13spring.java13springproject.film.dto.FilmDTO;
import com.sber.java13spring.java13springproject.film.dto.LoginDTO;
import com.sber.java13spring.java13springproject.film.dto.UserDTO;
import com.sber.java13spring.java13springproject.film.model.User;
import com.sber.java13spring.java13springproject.film.service.UserService;
import com.sber.java13spring.java13springproject.film.service.userdetails.CustomUserDetailsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;



import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи",
        description = "Контроллер для работы с пользователями")
public class UserController
        extends GenericController<User, UserDTO> {

    private final UserService userService;
    private final CustomUserDetailsService customUserDetailsService;
    private final JWTTokenUtil jwtTokenUtil;


    public UserController(UserService userService, CustomUserDetailsService customUserDetailsService, JWTTokenUtil jwtTokenUtil) {
        super(userService);
        this.userService = userService;
        this.customUserDetailsService = customUserDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Operation(description = "Получить список всех фильмов пользователя", method = "getAllFilms")
    @RequestMapping(value = "/getAllFilms", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<FilmDTO>> addFilm(@RequestParam(value = "id") Long id) {

            return ResponseEntity.status(HttpStatus.OK).body(userService.getFilmList(id));
    }

    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
        Map<String, Object> response = new HashMap<>();
        log.info("LoginDTO: {}", loginDTO);
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDTO.getLogin());
        log.info("foundUser, {}", foundUser);
        if (!userService.checkPassword(loginDTO.getPassword(), foundUser)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации!\nНеверный пароль");
        }
        String token = jwtTokenUtil.generateToken(foundUser);
        response.put("token", token);
        response.put("username", foundUser.getUsername());
        response.put("authorities", foundUser.getAuthorities());
        return ResponseEntity.ok().body(response);
    }
}
