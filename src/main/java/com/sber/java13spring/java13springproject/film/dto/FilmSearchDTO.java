package com.sber.java13spring.java13springproject.film.dto;

import com.sber.java13spring.java13springproject.film.model.Genre;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilmSearchDTO {
    private String filmTitle;
    private String directorFio;
    private Genre genre;
}
