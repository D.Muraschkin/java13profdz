package com.sber.java13spring.java13springproject.film.repository;

import com.sber.java13spring.java13springproject.film.model.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmRepository
        extends GenericRepository<Film> {
    @Query(nativeQuery = true,
            value = """
                 select distinct f.*
                 from films f
                 left join film_directors fd on f.id = fd.film_id
                 join directors d on d.id = fd.directors_id
                 where f.title ilike '%' || :title || '%'
                 and cast(f.genre as char) like coalesce(:genre,'%')
                 and d.directors_fio ilike '%' || :fio || '%'
                      """)
//    List
    Page
            <Film> searchFilms(@Param(value = "genre") String genre,
                           @Param(value = "title") String title,
                           @Param(value = "fio") String fio,
                           Pageable pageable);

}
