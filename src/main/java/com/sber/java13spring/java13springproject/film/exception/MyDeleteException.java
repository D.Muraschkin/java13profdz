package com.sber.java13spring.java13springproject.film.exception;

public class MyDeleteException
      extends Exception {
    public MyDeleteException(String message) {
        super(message);
    }
}
