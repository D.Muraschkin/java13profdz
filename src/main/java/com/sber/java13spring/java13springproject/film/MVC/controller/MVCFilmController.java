package com.sber.java13spring.java13springproject.film.MVC.controller;

import com.sber.java13spring.java13springproject.film.dto.*;
import com.sber.java13spring.java13springproject.film.service.DirectorService;
import com.sber.java13spring.java13springproject.film.service.FilmService;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/films")
@Hidden
public class MVCFilmController {
    private final FilmService filmService;
    private final DirectorService directorService;

        public MVCFilmController(FilmService filmService, DirectorService directorService) {
            this.filmService = filmService;
            this.directorService = directorService;
        }

        @GetMapping("")
        public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                             @RequestParam(value = "size", defaultValue = "5") int pageSize,
                             Model model) {
            PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "filmTitle"));

            Page<FilmWithDirectorsDTO> result = filmService.getAllFilmsWithDirectors(pageRequest);

            List<DirectorWithFilmsDTO> directors = directorService.getAllDirectorsWithFilms();
            model.addAttribute("films", result);
            model.addAttribute("directors", directors);
            return "films/viewAllFilms";
        }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("film", filmService.getFilmsWithDirectors(id));
        return "films/viewFilm";
    }

        //Рисует форму создания фильма
        @GetMapping("/add")
        public String create() {
            return "films/addFilm";
        }

        // Примит данные о созданном фильме и передаст в БД
        // Потом вернёт нас на страницу со всеми фильмами
        @PostMapping("/add")
        public String create(@ModelAttribute("filmForm") FilmDTO filmDTO) {
            filmService.create(filmDTO);
            return "redirect:/films";
        }
    @PostMapping("{filmId}/addDirector")
    public String addDirector(@ModelAttribute("addDirectorForm") DirectorDTO director,
                              @PathVariable("filmId") Long filmId) {

        filmService.addDirector(filmId, director.getId());
        return "redirect:/films";
    }
    @PostMapping("/search")
    public String searchBooks(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("filmSearchForm") FilmSearchDTO filmSearchDTO,
                              Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        model.addAttribute("films", filmService.findFilm(filmSearchDTO, pageRequest));
        return "films/viewAllFilms";
    }
}
