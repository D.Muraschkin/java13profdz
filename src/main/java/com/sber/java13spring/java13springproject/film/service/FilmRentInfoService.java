package com.sber.java13spring.java13springproject.film.service;

import com.sber.java13spring.java13springproject.film.dto.FilmDTO;
import com.sber.java13spring.java13springproject.film.dto.FilmRentInfoDTO;
import com.sber.java13spring.java13springproject.film.mapper.FilmRentInfoMapper;
import com.sber.java13spring.java13springproject.film.model.FilmRentInfo;
import com.sber.java13spring.java13springproject.film.repository.FilmRentInfoRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class FilmRentInfoService extends GenericService<FilmRentInfo, FilmRentInfoDTO> {

    private final FilmService filmService;
    private final FilmRentInfoMapper filmRentInfoMapper;


    public FilmRentInfoService(FilmRentInfoRepository filmRentInfoRepository,
                               FilmRentInfoMapper filmRentInfoMapper,
                               FilmService filmService) {
        super(filmRentInfoRepository, filmRentInfoMapper);
        this.filmService = filmService;
        this.filmRentInfoMapper = filmRentInfoMapper;
    }

    public FilmRentInfoDTO rentFilm(FilmRentInfoDTO rentFilmDTO) {
        FilmDTO filmDTO = filmService.getOne(rentFilmDTO.getFilmId());
//        filmDTO.setAmount(filmDTO.getAmount() - 1);
        filmService.update(filmDTO);
        long rentPeriod = rentFilmDTO.getRentPeriod() != null ? rentFilmDTO.getRentPeriod() : 14L;
        rentFilmDTO.setRentDate(LocalDateTime.now());
        rentFilmDTO.setReturned(false);
        rentFilmDTO.setRentPeriod((int) rentPeriod);
        rentFilmDTO.setReturnDate(LocalDateTime.now().plusDays(rentPeriod));
        rentFilmDTO.setCreatedWhen(LocalDateTime.now());
        rentFilmDTO.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        return mapper.toDTO(repository.save(mapper.toEntity(rentFilmDTO)));
    }
    public void returnFilm(final Long id) {
        FilmRentInfoDTO filmRentInfoDTO = getOne(id);
        filmRentInfoDTO.setReturned(true);
        filmRentInfoDTO.setReturnDate(LocalDateTime.now());
        FilmDTO filmDTO = filmRentInfoDTO.getFilmDTO();
//        filmDTO.setAmount(filmDTO.getAmount() + 1);
        update(filmRentInfoDTO);
        filmService.update(filmDTO);
    }
}
