package com.sber.java13spring.java13springproject.film.mapper;

import com.sber.java13spring.java13springproject.film.dto.GenericDTO;
import com.sber.java13spring.java13springproject.film.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);

    D toDTO(E entity);

    List<E> toEntities(List<D> dtos);

    List<D> toDTOs(List<E> entities);
}
