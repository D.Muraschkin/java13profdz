package com.sber.java13spring.java13springproject.film.service;

import com.sber.java13spring.java13springproject.film.dto.OrderDTO;
import com.sber.java13spring.java13springproject.film.mapper.OrderMapper;
import com.sber.java13spring.java13springproject.film.model.Film;
import com.sber.java13spring.java13springproject.film.model.Orders;
import com.sber.java13spring.java13springproject.film.model.User;
import com.sber.java13spring.java13springproject.film.repository.FilmRepository;
import com.sber.java13spring.java13springproject.film.repository.OrderInfoRepository;
import com.sber.java13spring.java13springproject.film.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDate;

@Service
public class OrderService extends GenericService<Orders, OrderDTO> {
    private OrderInfoRepository orderInfoRepository;
    private final UserRepository userRepository;
    private final FilmRepository filmRepository;


    public OrderService(OrderInfoRepository orderInfoRepository,
                        OrderMapper orderMapper,
                        UserRepository userRepository,
                        FilmRepository filmRepository) {
        super(orderInfoRepository, orderMapper);
        this.orderInfoRepository = orderInfoRepository;
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
    }

    //Сервис "Взять фильм в аренду (купить)"
    public OrderDTO orderFilm(OrderDTO orderDTO) {
        User user = userRepository.findById(orderDTO.getUserId())
                .orElseThrow(() -> new NotFoundException("Пользователь с таким ID не найден"));
        Film film = filmRepository.findById(orderDTO.getFilmId())
                .orElseThrow(() -> new NotFoundException("Фильм с таким ID не найден"));
        Orders order = new Orders();
        order.setFilm(film);
        order.setUser(user);

        //Купить
        if (orderDTO.getPurchase())
            order.setPurchase(true);
            // аренда
        else {
            order.setPurchase(false);
            order.setRentDate(LocalDate.now().atStartOfDay());
            order.setRentPeriod(orderDTO.getRentPeriod());
        }
        orderInfoRepository.save(order);
        return mapper.toDTO(order);
    }
}
