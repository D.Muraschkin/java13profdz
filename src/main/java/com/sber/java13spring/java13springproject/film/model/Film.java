package com.sber.java13spring.java13springproject.film.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "films")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "films_seq", allocationSize = 1)
public class Film
      extends GenericModel {
    
    @Column(name = "title", nullable = false)
    private String filmTitle;
    
    @Column(name = "premier_year", nullable = false)
    private int premierYear;

    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "genre", nullable = false)
    @Enumerated
    private Genre genre;
    
    @ManyToMany
    @JoinTable(name = "film_directors",
               joinColumns = @JoinColumn(name = "film_id"), foreignKey = @ForeignKey(name = "FK_FILM_DIRECTORS"),
               inverseJoinColumns = @JoinColumn(name = "directors_id"), inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILM"))

    private Set<Directors> directors;
    
    @OneToMany(mappedBy = "film")
    private Set<Orders> orders;
    @OneToMany(mappedBy = "film", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<FilmRentInfo> filmRentInfos;

    @Column(name = "is_deleted", columnDefinition = "boolean default false")
    private boolean isDeleted;

    @Column(name = "deleted_when")
    private LocalDateTime deletedWhen;

    @Column(name = "deleted_by")
    private String deletedBy;

}
