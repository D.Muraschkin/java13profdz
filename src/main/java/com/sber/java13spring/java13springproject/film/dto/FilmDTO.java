package com.sber.java13spring.java13springproject.film.dto;

import com.sber.java13spring.java13springproject.film.model.Directors;
import com.sber.java13spring.java13springproject.film.model.Film;
import com.sber.java13spring.java13springproject.film.model.Genre;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO extends GenericDTO {


    private String filmTitle;
    private Integer premierYear;
    private String country;
    private Genre genre;
    private boolean isDeleted;


    private Set<Long> directorIds;
    private Set<Long> ordersIds;
    public FilmDTO(Film film) {
        FilmDTO filmDTO = new FilmDTO();
        //из entity делаем DTO
        filmDTO.setFilmTitle(film.getFilmTitle());
        filmDTO.setPremierYear(film.getPremierYear());
        filmDTO.setCountry(film.getCountry());
        filmDTO.setGenre(film.getGenre());
        filmDTO.setId(film.getId());

        Set<Directors> authors = film.getDirectors();
        Set<Long> authorIds = new HashSet<>();
        if (authors != null && authors.size() > 0) {
            authors.forEach(a -> authorIds.add(a.getId()));
        }
        filmDTO.setDirectorIds(authorIds);
    }
}
