package com.sber.java13spring.java13springproject.film.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public abstract class GenericDTO {
    private Long id;
    private  String createdBy;
    private LocalDateTime createdWhen;
}
