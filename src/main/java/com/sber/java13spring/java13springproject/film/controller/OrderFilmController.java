package com.sber.java13spring.java13springproject.film.controller;

import com.sber.java13spring.java13springproject.film.dto.FilmDTO;
import com.sber.java13spring.java13springproject.film.dto.OrderDTO;
import com.sber.java13spring.java13springproject.film.dto.UserDTO;
import com.sber.java13spring.java13springproject.film.model.Orders;
import com.sber.java13spring.java13springproject.film.repository.OrderInfoRepository;

import com.sber.java13spring.java13springproject.film.service.FilmService;
import com.sber.java13spring.java13springproject.film.service.OrderService;
import com.sber.java13spring.java13springproject.film.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rent/info")
@Tag(name = "Аренда фильма",
     description = "Контроллер для работы с арендой фильмов пользователям")
public class OrderFilmController
      extends GenericController<Orders, OrderDTO> {
    private final OrderService orderService;

    public OrderFilmController(OrderService orderService) {
        super(orderService);
        this.orderService = orderService;
    }


    @Operation(description = "Взять фильм в аренду", method = "create")
    @RequestMapping(value = "/addOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> create(@RequestBody OrderDTO newEntity, @RequestParam(value = "filmId") Long filmId,
                                           @RequestParam(value = "userId") Long userId) {


        return ResponseEntity.status(HttpStatus.CREATED).body(orderService.orderFilm(newEntity));
    }
}
