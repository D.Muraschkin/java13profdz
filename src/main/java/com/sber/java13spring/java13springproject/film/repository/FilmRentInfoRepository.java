package com.sber.java13spring.java13springproject.film.repository;

import com.sber.java13spring.java13springproject.film.model.FilmRentInfo;

public interface FilmRentInfoRepository extends GenericRepository<FilmRentInfo> {
}
